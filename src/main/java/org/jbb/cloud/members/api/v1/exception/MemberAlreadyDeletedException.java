package org.jbb.cloud.members.api.v1.exception;

public class MemberAlreadyDeletedException extends MembersException {

  public MemberAlreadyDeletedException(String id) {
    super(String.format("Member with id '%s' is already deleted", id));
  }

}
