package org.jbb.cloud.members.api.v1.exception;

public class MemberNotFoundException extends MembersException {

  public MemberNotFoundException(String id) {
    super(String.format("Member with id '%s' doesn't exist", id));
  }

}
