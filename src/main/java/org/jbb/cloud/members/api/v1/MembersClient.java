package org.jbb.cloud.members.api.v1;

import java.util.List;
import javax.validation.Valid;
import org.jbb.cloud.members.api.v1.exception.MemberAlreadyDeletedException;
import org.jbb.cloud.members.api.v1.exception.MemberNotFoundException;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "jbb-members", url = "${jbb.members.url:}")
interface MembersClient {

  String V1_MEMBERS = "/v1/members";

  @RequestMapping(path = V1_MEMBERS, method = RequestMethod.GET)
  List<MemberDto> getAllMembers();

  @RequestMapping(path = V1_MEMBERS + "/{id}", method = RequestMethod.GET)
  MemberDto getMember(@PathVariable("id") String id) throws MemberNotFoundException;

  @RequestMapping(path = V1_MEMBERS, method = RequestMethod.POST)
  MemberDto createMember(@RequestBody @Valid CreateMemberDto createMemberDto);

  @RequestMapping(path = V1_MEMBERS + "/{id}", method = RequestMethod.PUT)
  MemberDto editMember(@PathVariable("id") String id,
      @RequestBody @Valid EditMemberDto editMemberDto) throws MemberNotFoundException;

  @RequestMapping(path = V1_MEMBERS + "/{id}", method = RequestMethod.DELETE)
  void deleteMember(@PathVariable("id") String id) throws MemberNotFoundException, MemberAlreadyDeletedException;

}
