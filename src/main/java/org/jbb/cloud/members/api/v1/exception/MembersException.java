package org.jbb.cloud.members.api.v1.exception;

public class MembersException extends Exception {

  public MembersException(String message) {
    super(message);
  }

}
