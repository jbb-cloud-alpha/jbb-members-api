package org.jbb.cloud.members.api.v1;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CreateMemberDto {

  @NotBlank
  private String username;

  @NotBlank
  private String displayedName;

  @NotBlank
  private String password;

  @NotBlank
  @Email
  private String email;

}
