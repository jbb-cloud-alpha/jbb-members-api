package org.jbb.cloud.members.api.v1;

import java.time.LocalDateTime;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class MemberDto {

  private String id;

  @NotBlank
  private String username;

  private String displayedName;

  private String email;

  private LocalDateTime joinDateTime;
}
