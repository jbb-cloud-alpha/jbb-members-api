package org.jbb.cloud.members.api.v1;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.Validate;
import org.jbb.cloud.members.api.v1.exception.MemberAlreadyDeletedException;
import org.jbb.cloud.members.api.v1.exception.MemberNotFoundException;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberResource {

  private final MembersClient membersClient;

  @Retryable
  public List<MemberDto> getAllMembers() {
    return membersClient.getAllMembers();
  }

  @Retryable
  public MemberDto getMember(String id) throws MemberNotFoundException {
    return membersClient.getMember(id);
  }

  @Retryable
  public MemberDto createMember(CreateMemberDto createMemberDto) {
    return membersClient.createMember(createMemberDto);
  }

  @Retryable
  public MemberDto editMember(String id, EditMemberDto editMemberDto)
      throws MemberNotFoundException {
    return membersClient.editMember(id, editMemberDto);
  }

  @Retryable
  public void deleteMember(String id)
      throws MemberNotFoundException, MemberAlreadyDeletedException {
    membersClient.deleteMember(id);
  }
}
